import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorRegisterService {

  constructor(
    public storage: Storage
  ) {
    //
  }

  /**
   * Registra um erro.
   * @param error Erro a ser registrado.
   */
  async registerError(error: any) {
    console.error(error);

    // Registrar o erro para depois publicar
    let s = await this.getSequenceError();

    let r: ErrorRecord = { e: error.toString(), status: 0, sequence: s };

    return this.storage.set('error-' + s, r);
  }

  /**
   * Retorna o sequencial de erros.
   */
  async getSequenceError() {
    let s: number = await this.storage.get('sequence-error');

    if (s && s > 0) {
      s++;
    } else {
      s = 1;
    }

    await this.storage.set('sequence-error', s);

    return s;
  }
}

export class ErrorRecord {
  e: any;
  sequence: number;
  status: number;
}
