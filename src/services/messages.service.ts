import { CadastroResponse } from './../models/cadastro-response.model';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Injectable()
export class MessagesService {

  constructor(
    public toastController: ToastController,
    public alertController: AlertController) {
    //
  }

  msgSemConexao() {
    this.toastController.create({
      message: 'Sem conexão à internet - exibindo últimos dados consultados.',
      closeButtonText: 'OK',
      showCloseButton: true,
      duration: 5000
    }).present();
  }

  msgErrorString(msg: string) {
    this.alertController.create({
      message: msg,
      buttons: ['OK'],
      enableBackdropDismiss: true,
      title: 'Aviso'
    }).present();
  }

  msgError(r: CadastroResponse<any>) {
    let msg = r.message;

    if (r.errors && r.errors.length > 0) {
      r.errors.forEach((error) => {
        msg += '\n* ' + error.message + '\n';
      });
    }

    this.alertController.create({
      message: msg,
      buttons: ['OK'],
      enableBackdropDismiss: true,
      title: 'Aviso'
    }).present();
  }
}
