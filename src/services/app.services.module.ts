import { ErrorRegisterService } from './error-register.service';
import { MessagesService } from './messages.service';
import { InternetService } from './internet.service';
import { MeusDadosService } from './meus-dados.service';
import { NgModule } from '@angular/core';
import { LoginService } from './login.service';
import { HttpModule } from '@angular/http';
import { UrlApiService } from './url-api.service';
import { MinhasViagensService } from './minhas-viagens.service';

@NgModule({
  imports: [HttpModule],
  providers: [LoginService,
    UrlApiService,
    MeusDadosService,
    InternetService,
    MinhasViagensService,
    MessagesService,
    ErrorRegisterService]
})
export class AppServicesModule { }
