import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class InternetService {
  constructor(private network: Network) { }

  isActive() {
    return this.network.type != 'none';
  }
}
