import { FormatUtils } from './../utils/format-utils';
import { Utils } from './../utils/shared';
import { Lancamento } from './../models/lancamento.model';
import { CadastroResponse } from './../models/cadastro-response.model';
import { Projeto } from './../models/projeto.model';
import { Cliente } from './../models/cliente.model';
import { Http } from '@angular/http';
import { Viagem } from './../models/viagem.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from './abstract-crud.service';
import { LoginService } from './login.service';
import { Storage } from '@ionic/storage';
import { UrlApiService } from './url-api.service';
import { Categoria } from '../models/categoria.model';

@Injectable()
export class MinhasViagensService extends AbstractCrudService<Viagem> {

  constructor(
    @Inject(Http) _http: Http,
    @Inject(LoginService) loginService: LoginService,
    @Inject(Storage) storage: Storage,
    @Inject(UrlApiService) urlApiService: UrlApiService) {
    super(_http, loginService, urlApiService, storage);
  }

  getEntityName(): string {
    return "minhas-viagens";
  }

  /**
   * Finaliza o Relatório de Viagem
   * @param vo Entidade de fechamento
   */
  async  finalizar(vo: Viagem): Promise<CadastroResponse<Viagem>> {
    return this._http.post(this.getUrl() + '/finalizar',
      JSON.stringify(vo),
      { headers: await this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => (error || 'Server error')).toPromise();
  }

  /**
   * Indica se a viagem pode ser editada / alterada
   * @param viagem Viagem a ser checada
   */
  isPodeEditarViagem(viagem: Viagem): boolean {
    return !viagem ? false : !viagem.status || viagem.status == '' || viagem.status == 'Em Andamento' || viagem.status == 'Relatório rejeitado';
  }

  trataUrlNfe(urlNfe: string, lancamento: Lancamento, viagem: Viagem = null, isNovoLancamento: boolean = false) {
    let chaveNfe: string;
    let modeloNfe: string;
    let valor: number = 0.0;
    let cnpj: string;
    let data: string = Utils.getCurrentDate();
    let numeroNota: string;

    if (urlNfe) {
      if (urlNfe.startsWith('http') && urlNfe.indexOf('sefaz') > 0) {
        // NFC-e genérica
        let urlCampos = urlNfe.split('?')[1];
        // chNFe=51171014953723001434650020000468051000468054
        // &nVersao=100
        // &tpAmb=1
        // &cDest=00273645188
        // &dhEmi=323031372d31302d31315432313a34313a32362d30343a3030
        // &vNF=91.20&
        // vICMS=0.00&
        // digVal=733167307a4e484b4446534459565173365a32643545784c6155303d
        // &cIdToken=000002&cHashQRCode=1055792A8515F8C1691FF09A3622C690F984387F
        let dados = urlCampos.split('&');
        dados.forEach((dado) => {
          try {
            if (dado.startsWith('chNFe')) {
              chaveNfe = dado.split('=')[1];
            } else if (dado.startsWith('vNF')) {
              valor = parseFloat(dado.split('=')[1]);
            } else if (dado.startsWith('dhEmi')) {
              let dataTemp = Utils.hex2a(dado.split('=')[1]);
              data = dataTemp.substr(8, 2) + '/' + dataTemp.substr(5, 2) + '/' + dataTemp.substr(0, 4);
            }
          } catch (err) {
            console.error(err);
          }
        });

        modeloNfe = chaveNfe.substr(21, 2);

      } else if (urlNfe.toUpperCase().startsWith('CFE') || urlNfe.startsWith('35')) {
        // NFC-e de São Paulo
        if (urlNfe.toUpperCase().startsWith('CFE')) {
          chaveNfe = urlNfe.substr(2, 44);
        } else {
          chaveNfe = urlNfe.substr(1, 44);
        }

        modeloNfe = chaveNfe.substr(21, 2);
      }

      if (chaveNfe && chaveNfe.trim().length > 0) {
        if (!lancamento.recibo) {
          lancamento.recibo = {};
        }

        if (chaveNfe) {
          cnpj = chaveNfe.substr(6, 14);
          numeroNota = chaveNfe.substr(25, 9);

          // alert(modeloNfe);

          if (modeloNfe === '59') {
            // SAT-SP

            // No SAT, temos informações adicionais do valor e da data de emissao
            let dados = urlNfe.split('|');

            // Ler data da nota
            if (dados.length >= 2) {
              try {
                data = dados[1];
                data = data.substr(6, 2) + '/' + data.substr(4, 2) + '/' + data.substr(0, 4);
              } catch (err) {
                console.error(err);
                data = Utils.getCurrentDate();
              }
            }

            // Ler valor da nota
            if (dados.length >= 3) {
              try {
                valor = parseFloat(dados[2]);
              } catch (err) {
                console.error(err);
                valor = 0.0;
              }
            }
          } else if (modeloNfe === '65') {
            // NFC-e comum
            numeroNota = chaveNfe.substr(25, 9);
          }
        }

        // Salva informações
        lancamento.recibo.dataReferencia = data;
        lancamento.recibo.chaveNfe = chaveNfe;
        lancamento.recibo.urlNfe = urlNfe;
        lancamento.recibo.valor = valor;
        lancamento.recibo.cpfCnpjFornecedor = cnpj;
        lancamento.recibo.numeroDocumento = numeroNota;

        // Informações default da nota, caso vazias
        if (isNovoLancamento) {
          if (viagem && viagem.lancamentos) {
            lancamento.sequencial = (viagem.lancamentos.length + 1).toString();
          } else {
            lancamento.sequencial = '1';
          }

          lancamento.categoria = {};
          lancamento.tipoPeriodo = 'Data de Referência';
        };

        // Formato do numero
        lancamento.recibo.valorString = FormatUtils.formatMoney(lancamento.recibo.valor);

        // Formato da data
        lancamento.recibo.dataReferenciaPicker = FormatUtils.dataToDatePicker(lancamento.recibo.dataReferencia);

        lancamento.dataInicial = data;
        lancamento.dataInicialPicker = FormatUtils.dataToDatePicker(lancamento.dataInicial);
        lancamento.tipoPeriodo = 'Data de Referência';

        lancamento.valorLancamento = valor;
        lancamento.valorLancamentoString = FormatUtils.formatMoney(lancamento.valorLancamento);
      }
    }

    console.log(chaveNfe);
    console.log(lancamento);
  }

  //
  // CLIENTES
  //

  /**
    * Retorna a lista de clientes online
    */
  async getClientes(): Promise<Cliente[]> {
    return this._http.get(this.getUrl() + '/clientes',
      { headers: await this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => (error || 'Server error')).toPromise();
  }

  /**
   * Salva a lista de clientes offline.
   * @param clientes
   */
  async saveClientesOffline(clientes: Cliente[]): Promise<any> {
    return this.storage.set('clientesList', clientes);
  }

  /**
   * Retorna a lista de clientes salva previamente offline.
   */
  async getClientesOffline(): Promise<Cliente[]> {
    return this.storage.get('clientesList');
  }

  //
  // PROJETOS
  //

  /**
   * Retorna a lista de projetos online
   */
  async getProjetos(): Promise<Projeto[]> {
    return this._http.get(this.getUrl() + '/projetos',
      { headers: await this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => (error || 'Server error')).toPromise();
  }

  /**
   * Salva a lista de projetos offline.
   * @param projetos
   */
  async saveProjetosOffline(projetos: Projeto[]): Promise<any> {
    return this.storage.set('projetosList', projetos);
  }

  /**
   * Retorna lista de projetos salva previamente offline.
   */
  async getProjetosOffline(): Promise<Projeto[]> {
    return this.storage.get('projetosList');
  }

  //
  // CATEGORIAS
  //

  /**
   * Retorna a lista de categorias online.
   */
  async getCategorias(): Promise<Categoria[]> {
    return this._http.get(this.getUrl() + '/categorias',
      { headers: await this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => (error || 'Server error')).toPromise();
  }

  /**
   * Salva a lista de categorias offline
   */
  async saveCategoriasOffline(categorias: Categoria[]) {
    return this.storage.set('categoriasList', categorias);
  }

  /**
  * Retorna lista de categorias salva previamente offline.
  */
  async getCategoriasOffline(): Promise<Categoria[]> {
    return this.storage.get('categoriasList');
  }
}
