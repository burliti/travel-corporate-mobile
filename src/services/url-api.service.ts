import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class UrlApiService {
  isApp: boolean;

  constructor(private platform: Platform) {
    this.isApp = this.platform.is('core') || this.platform.is('mobileweb') ? false : true;
    // this.isApp = true;
  }

  /**
   * Retorna a URL padrão de api
   */
  getUrl(): string {
    // return "http://192.168.0.18";
    return this.isApp ? 'https://travelcorporate.com.br' : 'http://localhost:8080';
  }
}
