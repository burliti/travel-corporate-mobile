import { UrlApiService } from './url-api.service';
import { MeusDados } from './../models/meus-dados.model';
import { LoginService } from './login.service';
import { Observable } from 'rxjs/Rx';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class MeusDadosService {

  constructor(private _http: Http,
    private loginService: LoginService,
    private urlApiService: UrlApiService,
    private storage: Storage) {
  }

  async getHeaders(): Promise<Headers> {
    // cria uma instância de Headers
    const headers = new Headers();
    // Adiciona o tipo de conteúdo application/json
    headers.append('Content-Type', 'application/json');
    // Adiciona o token de autenticacao
    let sessaoUsuario = await this.loginService.getUsuarioAutenticado();

    if (sessaoUsuario != null) {
      headers.append('AUTHENTICATION_TOKEN', sessaoUsuario.chaveSessao);
    }

    return headers;
  }

  /**
   * Retorna os meus dados
   */
  async getMeusDados(): Promise<MeusDados> {
    return this._http
      .post(this.urlApiService.getUrl() + '/api/resources/meus-dados/v1',
      null,
      { headers: await this.getHeaders() })
      .timeout(10000)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error')).toPromise();
  }

  /**
   * Salva os dados no storage, para trabalhar offline.
   * @param meusDados
   */
  saveMeusDados(meusDados: MeusDados): Promise<any> {
    return this.storage.set('meusDados', meusDados);
  }

  /**
   * Retorna os meus dados, salvos offline.
   */
  getMeusDadosOffline(): Promise<MeusDados> {
    return this.storage.get('meusDados');
  }
}
