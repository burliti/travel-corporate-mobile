import { ErrorRegisterService } from './error-register.service';
import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(public errorService: ErrorRegisterService) {

  }

  handleError(err: any): void {
    this.errorService.registerError(err);
  }
}
