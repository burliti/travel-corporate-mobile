import { EntidadeOffline } from './../models/entidade-offline.model';
import { UrlApiService } from './url-api.service';
import { CadastroResponse } from './../models/cadastro-response.model';
import { ConsultaResponse } from './../models/consulta-response.model';
import { ConsultaRequest } from './../models/consulta-request.model';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import { Storage } from '@ionic/storage';

export abstract class AbstractCrudService<T> {

  constructor(
    public _http: Http,
    public loginService: LoginService,
    public urlApiService: UrlApiService,
    public storage: Storage) { }

  async getHeaders(): Promise<Headers> {
    // cria uma instância de Headers
    const headers = new Headers();

    // Adiciona o tipo de conteúdo application/json
    headers.append('Content-Type', 'application/json');

    // Adiciona o token de autenticacao
    let sessaoUsuario = await this.loginService.getUsuarioAutenticado();

    if (sessaoUsuario != null) {
      headers.append('AUTHENTICATION_TOKEN', sessaoUsuario.chaveSessao);
    }

    return headers;
  }

  /**
   * Nome da entidade, para montar a URL do serviço
   */
  abstract getEntityName(): string;

  getUrl() {
    return this.urlApiService.getUrl() + '/api/resources/' + this.getEntityName() + "/" + this.getVersion();
  }

  private extractData(res: Response) {
    const body = res.json();

    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      errMsg = body;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

  /**
     * Retorna uma lista para o DataGrid
     * @param consultaRequest Objeto com os parametros da consulta
     * @returns Promise com a resposta da consulta
     */
  async getList(consultaRequest: ConsultaRequest): Promise<ConsultaResponse<T>> {
    let h = await this.getHeaders();
    return this._http.post(this.getUrl() + '/pesquisar',
      JSON.stringify(consultaRequest),
      { headers: h })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error')).toPromise();
  }

  /**
   * Retorna uma lista para o DataGrid, com os dados offline.
   */
  async getListOffline(): Promise<ConsultaResponse<T>> {
    let s = await this.storage.get('list-' + this.getEntityName());
    if (!s) {
      s = {
        lista: [],
        total: 0
      };
    }
    return s;
  }

  /**
   * Salva a lista de dados offline
   * @param response Objeto de response
   */
  async saveListOffline(response: ConsultaResponse<T>) {
    await this.storage.set('list-' + this.getEntityName(), response);
  }

  /**
   * Salva a entidade offline.
   * @param vo Entidade para salvar
   * @param id id da Entidade
   * @param status da entidade. Default = 1.
   * 0 - Sincronizar
   * 1 - Sincronizado
   */
  async saveOffline(vo: T, id: any, status: number = 1) {
    await this.storage.set(this.getEntityReference(id), new EntidadeOffline<T>(vo, id));
  }

  /**
   * Indica se a entidade está pendente de sincronismo offline.
   * @param id Id da entidade offline
   */
  async isSincronismoPendente(id: any) {
    let r = await this.storage.get(this.getEntityReference(id));
    return r.status == 0;
  }

  /**
   * Retorna a entidade pelo ID.
   */
  async buscar(id: any): Promise<T> {
    let h = await this.getHeaders();
    return this._http.get(this.getUrl() + '/' + id, { headers: h })
      .map(res => res.json())
      .toPromise();
  }

  /**
   * Retorna a entidade pelo ID, dados offline.
   */
  async buscarOffline(id: any): Promise<EntidadeOffline<T>> {
    return await this.storage.get(this.getEntityReference(id));
    // if (r && r.vo) {
    //   return r.vo;
    // } else {
    //   return null;
    // }
  }

  /**
   * Busca referencia da entidade, offline.
   * @param id Id da entidade.
   */
  private getEntityReference(id: any): string {
    return this.getEntityName() + '-' + id + '-' + this.getVersion();
  }

  /**
   * Excluir a entidade
   */
  async excluir(id: any): Promise<CadastroResponse<T>> {
    return this._http.delete(this.getUrl() + '/' + id, { headers: await this.getHeaders() })
      .map(this.extractData)
      .catch(this.handleError).toPromise();
  }

  /**
   * Atualizar a entidade
   */
  async atualizar(vo: T, id: any): Promise<CadastroResponse<T>> {
    return this._http.put(this.getUrl(), JSON.stringify(vo), { headers: await this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error')).toPromise();
  }

  /**
   * Inserir a entidade
   */
  async inserir(vo: T): Promise<CadastroResponse<T>> {
    const vo2: any = vo;

    return this._http.post(this.getUrl(), JSON.stringify(vo2), { headers: await this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error')).toPromise();
  }

  private getVersion() {
    return "v1";
  }
}
