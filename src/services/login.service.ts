import { UrlApiService } from './url-api.service';
import { SessaoUsuario } from './../models/sessao.model';
import { CadastroResponse } from './../models/cadastro-response.model';
import { Observable } from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Md5 } from 'ts-md5/dist/md5';
import 'rxjs/Rx';

@Injectable()
export class LoginService {

  // public usuarioAutenticado: UsuarioSessao;
  headers: Headers;

  private timerStarted = false;

  fotoChange: EventEmitter<string> = new EventEmitter();
  logoffEvent: EventEmitter<string> = new EventEmitter();
  loginEvent: EventEmitter<SessaoUsuario> = new EventEmitter();

  constructor(
    private http: Http,
    private urlApiService: UrlApiService,
    private storage: Storage) {
    // cria uma instância de Headers
    this.headers = new Headers();

    // Adiciona o tipo de conteúdo application/json
    this.headers.append('Content-Type', 'application/json');

    this.timerStarted = false;
  }

  /**
   * Método de autenticacao
   * @param email Email a ser autenticado
   * @param senha Senha do usuario
   */
  async entrar(email: string, senha: string): Promise<CadastroResponse<SessaoUsuario>> {
    const senhaMD5 = Md5.hashStr(senha);

    const dados = {
      'email': email,
      'senhaMD5': senhaMD5,
      'tipoSessao': 'Mobile'
    };

    return this.http.post(this.urlApiService.getUrl() + '/api/resources/acesso/v1',
      JSON.stringify(dados), { headers: this.headers })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error')).toPromise();
  }

  /**
   * Serviço para alterar a senha do funcionário
   * @param senha Senha atual do funcionário
   * @param novaSenha nova senha
   * @param confirmacaoNovaSenha confirmação da nova senha
   */
  async alterarSenha(senha: string, novaSenha: string, confirmacaoNovaSenha: string, chaveSessao: string = null): Promise<any> {
    // cria uma instância de Headers
    const headersAlterarSenha = new Headers();
    // Adiciona o tipo de conteúdo application/json
    headersAlterarSenha.append('Content-Type', 'application/json');
    // Adiciona o token de autenticacao
    let sessaoUsuario = await this.getUsuarioAutenticado();

    if (sessaoUsuario != null) {
      headersAlterarSenha.append('AUTHENTICATION_TOKEN', sessaoUsuario.chaveSessao);
    } else if (chaveSessao != null) {
      headersAlterarSenha.append('AUTHENTICATION_TOKEN', chaveSessao);
    }

    const dados = {
      'senha': senha,
      'novaSenha': novaSenha,
      'confirmacaoNovaSenha': confirmacaoNovaSenha
    };

    return this.http.post(this.urlApiService.getUrl() + '/api/resources/acesso/v1/alterarSenha', JSON.stringify(dados), { headers: headersAlterarSenha })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json() || 'Server error')).toPromise();
  }

  /**
   * Logoff da aplicacao.
   */
  logoff() {
    this.setUsuarioAutenticado(null);
    this.logoffEvent.emit('Logoff');
  }

  /**
   * Indica se o usuário está autenticado
   * @returns True se o usuário existir
   */
  async isAutenticado(): Promise<boolean> {
    const u = await this.getUsuarioAutenticado();

    try {
      if (u != null && u !== undefined && u.chaveSessao != null && u.chaveSessao !== undefined && u.chaveSessao != '') {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      console.error(e);

      return false;
    }
  }

  /**
   * Retorna o usuário autenticado.
   */
  async getUsuarioAutenticado(): Promise<SessaoUsuario> {
    let json = await this.storage.get('usuario');

    if (json) {
      const usuarioAutenticado: SessaoUsuario = json;
      return usuarioAutenticado;
    } else {
      return {};
    }
  }

  /**
   * Seta o usuário autenticado. Enviar null para remover.
   * @param sessaoUsuario Sessão do usuário para salvar.
   */
  async setUsuarioAutenticado(sessaoUsuario: SessaoUsuario) {
    if (sessaoUsuario != null) {
      await this.storage.set('usuario', sessaoUsuario);

      if (sessaoUsuario.funcionario && sessaoUsuario.funcionario.foto) {
        this.setFotoUsuario(sessaoUsuario.funcionario.foto);
      }
    } else {
      await this.storage.remove('usuario');
    }
  }

  async setFotoUsuario(foto: string) {
    this.fireEventFotoChange(foto);

    await this.storage.set('foto-usuario', foto);
  }

  async getFotoUsuario(): Promise<any> {
    return await this.storage.get('foto-usuario');
  }

  /**
   * Retorna email do usuário salvo anteriormente
   */
  async getEmailLembrar(): Promise<any> {
    return await this.storage.get("email_travelcorporate");
  }

  /**
   * Salva email nos cookies
   * @param email Email do usuário
   */
  setEmailLembrar(email: string) {
    this.storage.set("email_travelcorporate", email);
  }

  /**
   * Limpa o email dos cookies
   */
  clearEmailLembrar() {
    this.storage.remove("email_travelcorporate");
  }

  /**
   * Checa se a sessão ainda está válida
   */
  chekcAutentication() {
    // TODO checar o cookie, se o usuário está autenticado, para recuperar a sessao do mesmo.
  }

  /**
   * Evento para mudança da foto.
   */
  getEventFotoChange(): EventEmitter<string> {
    return this.fotoChange;
  }

  /**
   * Dispara o evento de change da foto
   * @param foto Foto a ser enviada.
   */
  fireEventFotoChange(foto: string) {
    this.fotoChange.emit(foto);
  }

  /**
   * Evento de logoff.
   */
  getEventLogoff(): EventEmitter<string> {
    return this.logoffEvent;
  }
}
