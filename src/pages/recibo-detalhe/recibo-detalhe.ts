import { MinhasViagensService } from './../../services/minhas-viagens.service';
import { InternetService } from './../../services/internet.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CurrencyUtils } from './../../utils/currency-utils';
import { FormatUtils } from './../../utils/format-utils';
import { Lancamento } from './../../models/lancamento.model';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-recibo-detalhe',
  templateUrl: 'recibo-detalhe.html',
})
export class ReciboDetalhePage {

  @ViewChild(Content) content: Content;

  public lancamento: Lancamento = {
    recibo: {}
  };
  public isNovo: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public currencyUtils: CurrencyUtils,
    public camera: Camera,
    public internet: InternetService,
    public minhasViagensService: MinhasViagensService,
    public barcodeScanner: BarcodeScanner) {
  }

  changeModelDataReferencia($event) {
    this.lancamento.recibo.dataReferencia = FormatUtils.datePickerToData(this.lancamento.recibo.dataReferenciaPicker);
  }

  keyUpValor() {
    this.lancamento.recibo.valorString = this.currencyUtils.detectAmount(this.lancamento.recibo.valorString);
    this.lancamento.recibo.valor = parseFloat(this.lancamento.recibo.valorString.replace(',', '.'));
  }

  ionViewDidLoad() {
    console.log('Enter');

    this.isNovo = this.navParams.get('novo');
    this.lancamento = this.navParams.get('lancamento');

    if (!this.lancamento.recibo) {
      this.lancamento.recibo = {};
      this.lancamento.recibo.dataReferencia = this.lancamento.dataInicial;
      this.lancamento.recibo.valor = this.lancamento.valorLancamento;
    }

    // Formato do numero
    this.lancamento.recibo.valorString = FormatUtils.formatMoney(this.lancamento.valorLancamento);

    // Formato da data
    this.lancamento.recibo.dataReferenciaPicker = FormatUtils.dataToDatePicker(this.lancamento.recibo.dataReferencia);
  }

  voltar() {
    this.navCtrl.pop();
  }

  salvar() {
    // Codigo para salvar
    this.voltar();
  }

  tirarFoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.lancamento.recibo.fotoAnexoRecibo = base64Image;
    }, (err) => {
      // Handle error
    });
  }

  limparFoto() {
    this.lancamento.recibo.fotoAnexoRecibo = null;
    this.content.scrollToTop();
  }

  escanearQrCode() {
    this.barcodeScanner.scan({
      formats: 'QR_CODE',

    }).then((barcodeData) => {
      if (!barcodeData.cancelled) {
        this.minhasViagensService.trataUrlNfe(barcodeData.text, this.lancamento);
      }
    }, (err) => console.error(err))
      .catch((err2) => console.error(err2));
  }
}
