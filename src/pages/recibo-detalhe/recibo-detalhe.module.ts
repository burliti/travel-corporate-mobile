import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReciboDetalhePage } from './recibo-detalhe';
import { PipeModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ReciboDetalhePage,
  ],
  imports: [
    PipeModule,
    IonicPageModule.forChild(ReciboDetalhePage),
  ],
})
export class ReciboDetalhePageModule {}
