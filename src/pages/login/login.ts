import { LoginService } from './../../services/login.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AppVersion } from '@ionic-native/app-version';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  lembrarEmail: boolean = false;
  email: string = '';
  senha: string = '';
  foto: string = 'assets/images/sem-foto.jpg';
  public versao: string = '0.0.0';
  erro: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loginService: LoginService,
    public loadingCtrl: LoadingController,
    public alertController: AlertController,
    public appVersion: AppVersion) {
  }

  async ionViewDidLoad() {
    setTimeout(() => {
      this.appVersion.getVersionNumber().then((v) => { this.versao = v }).catch((erro) => this.erro = erro);
    }, 1000);

    // try {
    //   this.versao = await this.appVersion.getVersionNumber();
    // } catch (erro) {
    //   this.erro = erro;
    // }

    this.carregarFoto();

    // Checa se o login está salvo
    if (await this.loginService.isAutenticado()) {
      this.redirecionarHome();
    } else {
      this.email = await this.loginService.getEmailLembrar();
    }

    // Inscreve no evento de mudança da foto
    this.loginService.getEventFotoChange().subscribe((novaFoto) => {
      this.foto = novaFoto;
    });
  }

  private carregarFoto() {
    this.loginService.getFotoUsuario().then((retornoFoto) => {
      if (retornoFoto && retornoFoto != '') {
        this.foto = retornoFoto;
      } else {
        this.foto = 'assets/images/sem-foto.jpg';
      }
    });
  }

  async entrar() {
    // Funcao de login
    let loading = this.loadingCtrl.create({
      content: 'Aguarde, autenticando...',
      enableBackdropDismiss: false
    });

    loading.present();

    try {
      let retorno = await this.loginService.entrar(this.email, this.senha);

      loading.dismiss();

      if (retorno.success) {
        this.loginService.setEmailLembrar(retorno.vo.funcionario.email);
        // Emite o evento de login com sucesso
        this.loginService.loginEvent.emit(retorno.vo);

        // Salva usuário autenticado
        this.loginService.setUsuarioAutenticado(retorno.vo);

        this.redirecionarHome();
      } else {
        this.alertController.create({
          message: retorno.message,
          buttons: ['Ok']
        }).present();
      }
    } catch (error) {

      console.log(error);

      this.alertController.create({
        message: 'Ocorreu um erro ao tentar fazer login! Verifique sua conexão de internet.',
        buttons: ['Ok']
      }).present();

      loading.dismiss();
    }
  }

  private redirecionarHome() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'forward' });
  }

  esqueciSenha() {

  }
}
