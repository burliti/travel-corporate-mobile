import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaAvatarPage } from './lista-avatar';

@NgModule({
  declarations: [
    ListaAvatarPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaAvatarPage),
  ],
})
export class ListaAvatarPageModule {}
