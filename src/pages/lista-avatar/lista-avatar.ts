import { LoginService } from './../../services/login.service';
import { InternetService } from './../../services/internet.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormatUtils } from '../../utils/format-utils';

/**
 * Generated class for the ListaAvatarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lista-avatar',
  templateUrl: 'lista-avatar.html',
})
export class ListaAvatarPage {

  avatars: string[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public internet: InternetService,
    public loginService: LoginService) {
  }

  ionViewDidLoad() {
    for (let i = 1; i < 11; i++) {
      this.avatars.push('assets/images/avatar/avatar' + FormatUtils.fill(i, '0', 2) + '.png');
    }
  }

  voltar() {
    this.navCtrl.pop();
  }

  clickAvatar(avatar: string) {
    this.loginService.setFotoUsuario(avatar);
    this.voltar();
  }
}
