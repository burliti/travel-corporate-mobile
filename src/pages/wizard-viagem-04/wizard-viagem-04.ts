import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { MinhasViagensService } from './../../services/minhas-viagens.service';
import { Viagem } from './../../models/viagem.model';
import { InternetService } from './../../services/internet.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MessagesService } from '../../services/messages.service';

@IonicPage()
@Component({
  selector: 'page-wizard-viagem-04',
  templateUrl: 'wizard-viagem-04.html',
})
export class WizardViagem_04Page {

  public isNovo: boolean = true;

  public viagem: Viagem = {
    cliente: {},
    projeto: {},
    fechamento: {},
    lancamentos: []
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public internet: InternetService,
    public minhasViagensService: MinhasViagensService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController,
    public msg: MessagesService) {
  }

  async ionViewDidEnter() {
    this.isNovo = this.navParams.get('novo');
    this.viagem = this.navParams.get('viagem');

    try {
      let r = await this.minhasViagensService.buscar(this.viagem.id);
      this.viagem = r;
    } catch (err) {
      this.msg.msgSemConexao();
    }
  }

  voltar() {
    this.navCtrl.pop();
  }

  async finalizar() {
    this.alertController.create({
      message: 'Deseja FINALIZAR o relatório de viagem? Esta ação não poderá ser desfeita!',
      buttons: [
        {
          text: 'Não',
          role: 'cancel'
        }, {
          text: 'Sim',
          handler: () => {
            this.confirmaFinalizar();
          }
        }]
    }).present();
  }

  isPodeEditar() {
    return this.minhasViagensService.isPodeEditarViagem(this.viagem);
  }

  public async confirmaFinalizar() {
    let loading = this.loadingController.create({
      content: 'Finalizando relatório...'
    });

    loading.present();

    try {
      let r = await this.minhasViagensService.finalizar(this.viagem);

      if (r.success) {
        this.toastController.create({
          message: r.message,
          showCloseButton: true,
          closeButtonText: 'OK',
          duration: 5000
        }).present();

        // Volta para a home
        this.navCtrl.goToRoot({

        });
      } else {
        this.msg.msgError(r);
      }
    } catch (error) {
      this.msg.msgSemConexao();
    }


    loading.dismiss();
  }
}
