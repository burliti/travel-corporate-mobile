import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WizardViagem_04Page } from './wizard-viagem-04';

@NgModule({
  declarations: [
    WizardViagem_04Page,
  ],
  imports: [
    IonicPageModule.forChild(WizardViagem_04Page),
  ],
})
export class WizardViagem_04PageModule {}
