import { UrlApiService } from './../../services/url-api.service';
import { InternetService } from './../../services/internet.service';
import { MeusDadosService } from './../../services/meus-dados.service';
import { MeusDados } from './../../models/meus-dados.model';
import { SessaoUsuario } from './../../models/sessao.model';
import { LoginPage } from './../login/login';
import { LoginService } from './../../services/login.service';
import { MinhasViagensPage } from './../minhas-viagens/minhas-viagens';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Page } from 'ionic-angular/navigation/nav-util';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  meusDados: MeusDados = {};
  viagensEmAbertoCount: number = 0;
  buscandoDados: boolean = false;
  public versao : string = '0.0.0';

  public sessaoUsuario: SessaoUsuario = {};

  constructor(
    public navCtrl: NavController,
    public alertController: AlertController,
    public loginService: LoginService,
    public meusDadosService: MeusDadosService,
    public toastController: ToastController,
    public network: Network,
    public urlApiService: UrlApiService,
    public internet: InternetService,
    public appVersion: AppVersion
  ) {
  }

  async ionViewDidLoad() {
    try {
      this.versao = await this.appVersion.getVersionNumber();
    } catch (error) {
    }


    this.sessaoUsuario = await this.loginService.getUsuarioAutenticado();

    if (!this.sessaoUsuario) {
      this.sair();
    }

    // Primeiro, carrega offline
    this.meusDados = await this.meusDadosService.getMeusDadosOffline();

    // Depois, carrega online
    if (this.internet.isActive()) {
      this.buscarMeusDados();
    } else {
      this.toastController.create({
        message: 'Sem conexão à internet - exibindo últimos dados consultados.',
        closeButtonText: 'OK',
        showCloseButton: true
      }).present();
    }
  }

  private async buscarMeusDados() {
    this.buscandoDados = true;

    try {
      this.meusDados = await this.meusDadosService.getMeusDados();

      this.meusDadosService.saveMeusDados(this.meusDados);
    } catch (error) {
      console.log(error);

      if (error && error.status) {
        if (error.status == 401 || error.status == 403) {
          this.loginService.logoff();

          this.toastController.create({
            message: 'Acesso expirado, favor fazer login novamente.',
            closeButtonText: 'OK',
            showCloseButton: true,
            duration: 5000
          }).present();

          return;
        }
      }

      this.toastController.create({
        message: 'Erro ao buscar meus dados! Verifique se a sua conexão está ativa.',
        closeButtonText: 'OK',
        showCloseButton: true,
        duration: 5000
      }).present();

    }

    this.buscandoDados = false;
  }

  openPage(page: Page) {
    this.navCtrl.push(page);
  }

  prestacaoDeContas() {
    this.openPage(MinhasViagensPage);
  }

  aprovacoes() {
    this.alertController.create({
      message: 'Função ainda não implementada!',
      buttons: ['OK']
    }).present();
  }

  cotacoes() {
    this.alertController.create({
      message: 'Função ainda não implementada!',
      buttons: ['OK']
    }).present();
  }

  relatorios() {
    this.alertController.create({
      message: 'Função ainda não implementada!',
      buttons: ['OK']
    }).present();
  }

  sair() {
    this.loginService.logoff();
    this.navCtrl.setRoot(LoginPage);
  }

}
