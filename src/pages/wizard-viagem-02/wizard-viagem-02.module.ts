import { PipeModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WizardViagem_02Page } from './wizard-viagem-02';

@NgModule({
  declarations: [
    WizardViagem_02Page
  ],
  imports: [
    IonicPageModule.forChild(WizardViagem_02Page),
    PipeModule
  ],
})
export class WizardViagem_02PageModule { }
