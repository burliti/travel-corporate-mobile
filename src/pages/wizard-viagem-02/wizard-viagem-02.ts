import { FormatUtils } from './../../utils/format-utils';
import { Viagem } from './../../models/viagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WizardViagem_03Page } from '../wizard-viagem-03/wizard-viagem-03';

@IonicPage()
@Component({
  selector: 'page-wizard-viagem-02',
  templateUrl: 'wizard-viagem-02.html',
})
export class WizardViagem_02Page {

  public isNovo: boolean = true;
  public viagem: Viagem = {
    cliente: {},
    projeto: {}
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //
  }

  avancar() {
    // Avancar
    this.navCtrl.push(WizardViagem_03Page, { 'viagem': this.viagem, 'novo': this.isNovo });
  }

  voltar() {
    this.navCtrl.pop();
  }

  async ionViewDidEnter() {
    this.isNovo = this.navParams.get('novo');

    if (!this.isNovo) {
      this.viagem = this.navParams.get('viagem');

      // Formato da data
      this.viagem.dataIdaPicker = FormatUtils.dataToDatePicker(this.viagem.dataIda);
      this.viagem.dataVoltaPicker = FormatUtils.dataToDatePicker(this.viagem.dataVolta);
    }
  }
}
