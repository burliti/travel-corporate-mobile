import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Recibo } from '../../models/recibo.model';
import { InternetService } from '../../services/internet.service';

@IonicPage()
@Component({
  selector: 'page-qr-code-scanner',
  templateUrl: 'qr-code-scanner.html',
})
export class QrCodeScannerPage {

  public recibo: Recibo = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public qrScanner: QRScanner,
    public internet: InternetService) {
  }

  ionViewDidLoad() {
    this.recibo = this.navParams.get("recibo");

    this.executarQrCode();
  }

  async ionViewDidLeave() {
    await this.qrScanner.hide();
    await this.qrScanner.destroy();
  }

  async voltar() {
    this.navCtrl.pop();
  }

  executarQrCode() {
    this.showToast("1");

    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          this.showToast("2");
          // camera permission was granted
          // start scanning
          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
            this.recibo.urlNfe = text;

            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning

            this.voltar();
          });

          // show camera preview
          (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');

          this.qrScanner.show()
            .then((data: QRScannerStatus) => {
              //
              // this.showToast("3");
            })
            .catch((reason) => {
              console.error(reason);
              // alert('Erro ao exibir QR Code Scanner!' + JSON.stringify(reason));
            });
        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there

          let toast = this.toastCtrl.create({
            message: 'Acesso negado permanentemente!',
            duration: 3000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });

          toast.present();
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.

          let toast = this.toastCtrl.create({
            message: 'Acesso negado!',
            duration: 3000,
            position: 'bottom'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });

          toast.present();
        }
      })
      .catch((e: any) => {
        let toast = this.toastCtrl.create({
          message: 'Erro!' + JSON.stringify(e),
          duration: 3000,
          position: 'bottom'
        });

        toast.present();
      });
  }

  showToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    }).present();
  }
}
