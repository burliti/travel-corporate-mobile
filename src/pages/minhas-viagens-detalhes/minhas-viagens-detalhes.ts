import { FormatUtils } from './../../utils/format-utils';
import { Projeto } from './../../models/projeto.model';
import { Cliente } from './../../models/cliente.model';
import { Viagem } from './../../models/viagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MinhasViagensDetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-minhas-viagens-detalhes',
  templateUrl: 'minhas-viagens-detalhes.html'
})
export class MinhasViagensDetalhesPage {

  public viagem: Viagem = {
    cliente: {},
    projeto: {}
  };

  public isNovo: boolean = true;
  public clientes: Cliente[] = [];
  public projetos: Projeto[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // Clientes
    for (let i = 0; i < 40; i++) {
      this.clientes.push({
        id: (i + 1),
        nome: 'Cliente ' + (i + 1).toString()
      });
    }

    // Projetos
    for (let i = 0; i < 100; i++) {
      this.projetos.push({
        id: (i + 1),
        nome: 'Projeto ' + (i + 1).toString()
      });
    }
  }

  changeModelDataIda($event) {
    this.viagem.dataIda = FormatUtils.datePickerToData(this.viagem.dataIdaPicker);
  }

  changeModelDataVolta($event) {
    this.viagem.dataVolta = FormatUtils.datePickerToData(this.viagem.dataVoltaPicker);
  }

  public optionsDataIda(): any {
    return {
      buttons: [{
        text: 'LimparX',
        handler: () => {
          this.viagem.dataIdaPicker = null;
          this.viagem.dataIda = null;
          return false;
        }
      }]
    };
  }

  ionViewDidLoad() {
    this.isNovo = this.navParams.get('novo');

    if (!this.isNovo) {
      this.viagem = this.navParams.get('viagem');

      // Formato da data
      this.viagem.dataIdaPicker = FormatUtils.dataToDatePicker(this.viagem.dataIda);
      this.viagem.dataVoltaPicker = FormatUtils.dataToDatePicker(this.viagem.dataVolta);
    }
  }


}

