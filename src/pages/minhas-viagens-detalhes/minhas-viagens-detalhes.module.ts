import { PipeModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhasViagensDetalhesPage } from './minhas-viagens-detalhes';

@NgModule({
  declarations: [
    MinhasViagensDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhasViagensDetalhesPage),
    PipeModule
  ],
})
export class MinhasViagensDetalhesPageModule {}
