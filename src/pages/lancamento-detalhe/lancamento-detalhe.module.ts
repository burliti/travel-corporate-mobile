import { PipeModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LancamentoDetalhePage } from './lancamento-detalhe';

@NgModule({
  declarations: [
    LancamentoDetalhePage,
  ],
  imports: [
    PipeModule,
    IonicPageModule.forChild(LancamentoDetalhePage),
  ],
})
export class LancamentoDetalhePageModule { }
