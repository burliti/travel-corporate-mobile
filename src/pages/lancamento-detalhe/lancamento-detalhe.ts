import { Utils } from './../../utils/shared';
import { MessagesService } from './../../services/messages.service';
import { MinhasViagensService } from './../../services/minhas-viagens.service';
import { Categoria } from './../../models/categoria.model';
import { Lancamento } from './../../models/lancamento.model';
import { FormatUtils } from './../../utils/format-utils';
import { Viagem } from './../../models/viagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CurrencyUtils } from '../../utils/currency-utils';
import { ReciboDetalhePage } from '../recibo-detalhe/recibo-detalhe';
import { InternetService } from '../../services/internet.service';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@IonicPage()
@Component({
  selector: 'page-lancamento-detalhe',
  templateUrl: 'lancamento-detalhe.html',
})
export class LancamentoDetalhePage {

  public isNovo: boolean = true;
  public viagem: Viagem = {
    cliente: {},
    projeto: {},
    lancamentos: []
  };

  public lancamento: Lancamento = {
    categoria: {}
  };

  public categorias: Categoria[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public currencyUtils: CurrencyUtils,
    public internet: InternetService,
    public minhasViagensService: MinhasViagensService,
    public msg: MessagesService,
    public loadingController: LoadingController) {
  }

  isPodeEditar() {
    return this.minhasViagensService.isPodeEditarViagem(this.viagem);
  }

  async salvar() {

    // Validação local
    if (!this.lancamento.valorLancamento || this.lancamento.valorLancamento <= 0) {
      this.msg.msgErrorString('Informe o valor!');
      return;
    }

    if (!this.lancamento.categoria || !this.lancamento.categoria.id || this.lancamento.categoria.id == '-1') {
      this.msg.msgErrorString('Informe a categoria do lançamento!');
      return;
    }

    if (!this.lancamento.dataInicial) {
      if (this.lancamento.tipoPeriodo == 'Data de Referência') {
        this.msg.msgErrorString('Informe a data do lançamento!');
      } else {
        this.msg.msgErrorString('Informe a data inicial!');
      }
      return;
    }

    let loading = this.loadingController.create({
      content: 'Aguarde, processando...'
    });

    loading.present();

    if (this.isNovo) {
      // Adiciona na lista
      if (!this.viagem.lancamentos) {
        this.viagem.lancamentos = [];
      }
      this.viagem.lancamentos.push(this.lancamento);
      this.isNovo = false;
    }

    if (!this.internet.isActive()) {
      // Se estiver offline, salva os dados offline.
      await this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);
      this.voltar();
    } else {
      try {
        let r = await this.minhasViagensService.atualizar(this.viagem, this.viagem.id);

        if (r.success) {
          // Salva offline
          await this.minhasViagensService.saveOffline(r.vo, r.vo.id, 1);

          this.viagem = r.vo;

          this.voltar();
        } else {
          this.msg.msgError(r);
        }

      } catch (error) {
        await this.minhasViagensService.saveOffline(this.viagem, this.viagem.id, 0);

        this.msg.msgSemConexao();

        this.voltar();
      }
    }

    loading.dismiss();
  }

  recibo() {
    this.navCtrl.push(ReciboDetalhePage, { "lancamento": this.lancamento });
  }

  voltar() {
    this.navCtrl.pop();
  }

  changeModelDataInicial($event) {
    this.lancamento.dataInicial = FormatUtils.datePickerToData(this.lancamento.dataInicialPicker);
  }

  changeModelDataFinal($event) {
    this.lancamento.dataFinal = FormatUtils.datePickerToData(this.lancamento.dataFinalPicker);
  }

  changeCategoriaModel($event) {
    if (this.lancamento.categoria && this.lancamento.categoria.id) {
      this.categorias.forEach((c) => {
        if (c.id == this.lancamento.categoria.id) {
          this.lancamento.categoria = c;
          this.lancamento.tipoLancamento = this.lancamento.categoria.tipo;
        }
      });
    }
  }

  keyUpValorLancamento() {
    this.lancamento.valorLancamentoString = this.currencyUtils.detectAmount(this.lancamento.valorLancamentoString);
    this.lancamento.valorLancamento = parseFloat(this.lancamento.valorLancamentoString.replace(',', '.'));
  }

  async ionViewDidLoad() {
    console.log('Carregando lancamento...');
    // Primeiro, busca as categorias offline
    let c = await this.minhasViagensService.getCategoriasOffline();

    if (!c) {
      this.categorias = [];
    } else {
      this.categorias = c;
    }

    try {
      // Busca online
      let categoriasOnline = await this.minhasViagensService.getCategorias();
      if (categoriasOnline) {
        this.categorias = categoriasOnline;
      }

      // Se deu certo, salva offline.
      await this.minhasViagensService.saveCategoriasOffline(this.categorias);
    } catch (error) {
      this.msg.msgSemConexao();
    }

    this.isNovo = this.navParams.get('novo');
    this.viagem = this.navParams.get('viagem');
    let l: Lancamento = this.navParams.get('lancamento');

    if (!l) {
      if (!this.viagem.lancamentos) {
        this.viagem.lancamentos = [];
      }

      this.lancamento = {
        sequencial: (this.viagem.lancamentos.length + 1).toString(),
        categoria: {},
        tipoPeriodo: 'Data de Referência',
        dataInicial: Utils.getCurrentDate(),
        valorLancamento: 0
      };
    } else {
      if (!l.categoria) {
        l.categoria = {};
      }
      this.lancamento = l;
    }

    // Formato do numero
    this.lancamento.valorLancamentoString = FormatUtils.formatMoney(this.lancamento.valorLancamento);

    // Formato da data
    this.lancamento.dataInicialPicker = FormatUtils.dataToDatePicker(this.lancamento.dataInicial);
    this.lancamento.dataFinalPicker = FormatUtils.dataToDatePicker(this.lancamento.dataFinal);
  }
}
