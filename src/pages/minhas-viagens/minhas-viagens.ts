import { ConsultaResponse } from './../../models/consulta-response.model';
import { MeusDadosService } from './../../services/meus-dados.service';
import { MeusDados } from './../../models/meus-dados.model';
import { InternetService } from './../../services/internet.service';
import { WizardViagem_01Page } from './../wizard-viagem-01/wizard-viagem-01';
import { Viagem } from './../../models/viagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ItemSliding } from 'ionic-angular';
import { WizardViagem_03Page } from '../wizard-viagem-03/wizard-viagem-03';
import { MinhasViagensService } from '../../services/minhas-viagens.service';
import { LoginService } from '../../services/login.service';
import { MessagesService } from '../../services/messages.service';

@IonicPage()
@Component({
  selector: 'page-minhas-viagens',
  templateUrl: 'minhas-viagens.html',
})
export class MinhasViagensPage {

  /**
   * Inicialização da variável
   */
  public dados: ConsultaResponse<Viagem> = {
    lista: [],
    total: 0
  };

  meusDados: MeusDados = {};
  buscandoDados: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private toastController: ToastController,
    public meusDadosService: MeusDadosService,
    public minhasViagensService: MinhasViagensService,
    public loginService: LoginService,
    public internet: InternetService,
    public msg: MessagesService) {
  }

  detalhesViagem(viagem: Viagem, slidingItem: ItemSliding): void {
    slidingItem.close();
    this.navCtrl.push(WizardViagem_01Page, { 'viagem': viagem, 'novo': false });
  }

  async lancamentosViagem(viagem: Viagem, slidingItem: ItemSliding) {
    slidingItem.close();

    let v = await this.minhasViagensService.buscarOffline(viagem.id);
    if (v) {
      viagem = v.vo;
    }

    try {
      viagem = await this.minhasViagensService.buscar(viagem.id);
    } catch (error) {
      this.msg.msgSemConexao();
    }

    this.navCtrl.push(WizardViagem_03Page, { 'viagem': viagem, 'novo': false });
  }

  adicionarViagem() {
    this.navCtrl.push(WizardViagem_01Page, {
      'novo': true
    });
  }

  async excluir(viagem: Viagem, index: number, slidingItem: ItemSliding) {
    slidingItem.close();

    this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja realmente EXCLUIR a viagem selecionada?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        }, {
          text: 'Confirmar',
          handler: () => {
            // Excluir a viagem
            this.minhasViagensService.excluir(viagem.id).then((retorno) => {
              if (retorno.success) {
                this.toastController.create({
                  message: 'Excluído!',
                  showCloseButton: true,
                  closeButtonText: 'Fechar'
                }).present();

                // Atualiza a página
                this.loadData(1);
              } else {
                this.msg.msgError(retorno);
              }
            }).catch((reason) => {
              this.msg.msgSemConexao();
            });
          }
        }
      ]
    }).present();
  }

  async ionViewDidEnter() {
    // Primeiro, carrega offline
    this.meusDados = await this.meusDadosService.getMeusDadosOffline();

    // Depois, carrega online
    if (this.internet.isActive()) {
      this.buscarMeusDados();
    } else {
      this.toastController.create({
        message: 'Sem conexão à internet - exibindo últimos dados consultados.',
        closeButtonText: 'OK',
        showCloseButton: true,
        duration: 5000
      }).present();
    }

    // Carrega lista de viagens
    this.loadData(1);
  }

  async doRefresh(refresher) {
    await this.loadData(1);

    refresher.complete();
  }

  /**
   * Atualiza meus dados (saldo)
   */
  private async buscarMeusDados() {
    this.buscandoDados = true;

    try {
      this.meusDados = await this.meusDadosService.getMeusDados();

      this.meusDadosService.saveMeusDados(this.meusDados);
    } catch (error) {
      this.toastController.create({
        message: 'Erro ao buscar meus dados! Verifique se a sua conexão está ativa.',
        closeButtonText: 'OK',
        showCloseButton: true,
        duration: 5000
      }).present();

      this.meusDados = await this.meusDadosService.getMeusDadosOffline();
    }

    this.buscandoDados = false;
  }

  /**
   * Carrega os dados offline e online
   * @param pageNumber Número da página
   */
  private async loadData(pageNumber: number) {
    // Exibe dados offline
    this.dados = await this.minhasViagensService.getListOffline();

    // Depois busca online
    if (this.internet.isActive()) {
      try {
        this.dados = await this.minhasViagensService.getList({
          filtro: {},
          order: { 'sequencial': 'desc' },
          pageNumber: pageNumber,
          pageSize: 5
        });

        this.minhasViagensService.saveListOffline(this.dados);
      } catch (error) {
        if (error.status == 401 || error.stauts == 403) {

          this.loginService.logoff();

          this.toastController.create({
            message: 'Acesso expirado, favor fazer login novamente.',
            closeButtonText: 'OK',
            showCloseButton: true,
            duration: 5000
          }).present();
        } else {
          this.toastController.create({
            message: 'Erro ao buscar minhas viagens! Verifique se a sua conexão está ativa.',
            closeButtonText: 'OK',
            showCloseButton: true,
            duration: 5000
          }).present();
        }

        // Exibe dados offline
        this.dados = await this.minhasViagensService.getListOffline();
      }
    } else {
      // TODO - Mostrar toast
      // Busca os dados offline primeiro
      this.dados = await this.minhasViagensService.getListOffline();
    }
  }

  getDataTratada(data) {
    return data ? data : 'não informado';
  }
}
