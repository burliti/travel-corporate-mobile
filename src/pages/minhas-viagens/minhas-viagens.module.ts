import { PipeModule } from './../../pipes/pipes.module';
import { MinhasViagensDetalhesPageModule } from './../minhas-viagens-detalhes/minhas-viagens-detalhes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhasViagensPage } from './minhas-viagens';

@NgModule({
  declarations: [
    MinhasViagensPage,
  ],
  imports: [
    PipeModule,
    IonicPageModule.forChild(MinhasViagensPage),
    MinhasViagensDetalhesPageModule
  ],
})
export class MinhasViagensPageModule {}
