import { ListaAvatarPage } from './../lista-avatar/lista-avatar';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Utils } from './../../utils/shared';
import { LoginService } from './../../services/login.service';
import { SessaoUsuario } from './../../models/sessao.model';
import { InternetService } from './../../services/internet.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';

@IonicPage()
@Component({
  selector: 'page-meus-dados',
  templateUrl: 'meus-dados.html',
})
export class MeusDadosPage {

  private sessaoUsuario: SessaoUsuario = {};

  novaSenha: string;
  confirmacaoNovaSenha: string;

  private foto: string = 'assets/images/sem-foto.jpg';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public internet: InternetService,
    public alertController: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public camera: Camera,
    public loginService: LoginService,
    public loadingCtrl: LoadingController) {
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad MeusDadosPage');

    this.sessaoUsuario = await this.loginService.getUsuarioAutenticado();

    // Busca a foto
    if (this.sessaoUsuario.funcionario && this.sessaoUsuario.funcionario.foto && this.sessaoUsuario.funcionario.foto != '') {
      this.foto = this.sessaoUsuario.funcionario.foto;
    } else {
      this.loginService.getFotoUsuario().then((retornoFoto) => {
        if (retornoFoto && retornoFoto != '') {
          this.foto = retornoFoto;
        } else {
          this.foto = 'assets/images/sem-foto.jpg';
        }
      });
    }

    // Inscreve no evento de mudança da foto
    this.loginService.getEventFotoChange().subscribe((novaFoto) => {
      this.foto = novaFoto;
    });
  }

  async getFoto(): Promise<string> {
    if (this.sessaoUsuario && this.sessaoUsuario.funcionario && this.sessaoUsuario.funcionario.foto) {
      return this.sessaoUsuario.funcionario.foto;
    } else if (this.loginService.getFotoUsuario()) {
      return this.loginService.getFotoUsuario();
    } else {
      return 'assets/images/sem-foto.jpg';
    }
  }

  clickFoto() {
    this.actionSheetCtrl.create({
      title: 'Selecione uma opção',
      buttons: [
        {
          text: 'Tirar foto',
          handler: () => {
            this.tirarFoto();
          }
        },
        {
          text: 'Escolher avatar',
          handler: () => {
            this.escolherAvatar();
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    }).present();
  }

  public escolherAvatar() {
    this.navCtrl.push(ListaAvatarPage);
  }

  public tirarFoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.sessaoUsuario.funcionario.foto = base64Image;

      // Seta foto do usuário
      this.loginService.setUsuarioAutenticado(this.sessaoUsuario);

    }, (err) => {
      console.log(err);
      // Handle error
    });
  }

  voltar() {
    this.navCtrl.pop();
  }

  async alterarSenha() {


    if (this.novaSenha && this.confirmacaoNovaSenha) {
      if (this.novaSenha === this.confirmacaoNovaSenha) {

        // Chama servico de alteracao de senhas
        let loader = this.loadingCtrl.create({
          content: 'Por favor, aguarde...',
          spinner: 'crescent'
        });

        loader.present();

        try {
          let retorno = await this.loginService.alterarSenha(null, this.novaSenha, this.confirmacaoNovaSenha, this.sessaoUsuario.chaveSessao);

          loader.dismiss();

          if (retorno.success) {
            this.alertController.create({
              message: 'Senha alterada com sucesso!',
              buttons: ['OK']
            }).present();

            this.novaSenha = '';
            this.confirmacaoNovaSenha = '';
          } else {
            let msg = Utils.buildErrorMessage(retorno);

            this.alertController.create({
              message: msg,
              buttons: ['OK']
            }).present();
          }
        } catch (error) {
          loader.dismiss;

          this.alertController.create({
            message: 'Ocorreu um erro ao alterar sua senha!',
            buttons: ['OK']
          }).present();
        }


      } else {
        this.alertController.create({
          message: 'Senha e nova senha devem ser iguais!',
          buttons: ['OK']
        }).present();
      }
    } else {
      this.alertController.create({
        message: 'Informe a senha e a nova senha!',
        buttons: ['OK']
      }).present();
    }
  }
}
