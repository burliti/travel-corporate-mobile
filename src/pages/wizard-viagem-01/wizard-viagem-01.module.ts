import { PipeModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WizardViagem_01Page } from './wizard-viagem-01';

@NgModule({
  declarations: [
    WizardViagem_01Page,
  ],
  imports: [
    IonicPageModule.forChild(WizardViagem_01Page),
    PipeModule
  ],
})
export class WizardViagem_01PageModule {}
