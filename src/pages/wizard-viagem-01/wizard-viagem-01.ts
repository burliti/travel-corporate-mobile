import { Utils } from './../../utils/shared';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { MessagesService } from './../../services/messages.service';
import { InternetService } from './../../services/internet.service';
import { Projeto } from './../../models/projeto.model';
import { Cliente } from './../../models/cliente.model';
import { FormatUtils } from './../../utils/format-utils';
import { Viagem } from './../../models/viagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MinhasViagensService } from '../../services/minhas-viagens.service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { WizardViagem_03Page } from '../wizard-viagem-03/wizard-viagem-03';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Lancamento } from '../../models/lancamento.model';

@IonicPage()
@Component({
  selector: 'page-wizard-viagem-01',
  templateUrl: 'wizard-viagem-01.html',
})
export class WizardViagem_01Page {

  public isNovo: boolean = true;
  public viagem: Viagem = {
    cliente: {},
    projeto: {}
  };

  public clientes: Cliente[] = [];
  public projetos: Projeto[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public minhasViagensService: MinhasViagensService,
    public internet: InternetService,
    public toastController: ToastController,
    public msg: MessagesService,
    public loadingCtrl: LoadingController,
    public camera: Camera,
    public barcodeScanner: BarcodeScanner) {
  }

  isPodeEditar() {
    return this.minhasViagensService.isPodeEditarViagem(this.viagem);
  }

  async avancar() {
    this.salvar(true);
  }

  async salvar(isAvancar: boolean = false) {
    // Se nao pode editar, apenas avança nas telas...
    if (!this.isPodeEditar()) {
      if (isAvancar) {
        this.navegarAvancar();
      }
      return;
    }

    if (!this.viagem.descricao || this.viagem.descricao.trim().length <= 0) {
      this.msg.msgErrorString('Informe a descrição da viagem!');
      return;
    }

    let loading = this.loadingCtrl.create({
      content: 'Aguarde, carregando...',
      enableBackdropDismiss: false
    });

    loading.present();

    // 1 - Tenta salvar online os dados
    if (this.isNovo) {
      try {
        let response = await this.minhasViagensService.inserir(this.viagem);

        if (response.success) {

          this.tratamentoModel(response.vo);

          this.viagem = response.vo;

          this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

          this.isNovo = false;

          if (isAvancar) {
            this.navegarAvancar();
          }
        } else {
          this.msg.msgError(response);
        }
      } catch (error) {
        // Erro genérico
        this.msg.msgSemConexao();

        // Salva os dados offline
        this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

        // Avança para a próxima tela
        if (isAvancar) {
          this.navegarAvancar();
        }
      }

    } else {
      try {
        let response = await this.minhasViagensService.atualizar(this.viagem, this.viagem.id);

        if (response.success) {
          this.tratamentoModel(response.vo);

          this.viagem = response.vo;

          this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

          if (isAvancar) {
            this.navegarAvancar();
          }
        } else {
          this.msg.msgError(response);
        }

      } catch (error) {
        // Erro genérico
        this.msg.msgSemConexao();

        // Salva os dados offline
        this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

        // Avança para a próxima tela
        if (isAvancar) {
          this.navegarAvancar();
        }
      }
    }

    loading.dismiss();
  }

  navegarAvancar(lancamento: Lancamento = null) {
    this.navCtrl.push(WizardViagem_03Page, { 'viagem': this.viagem, 'novo': this.isNovo, 'lancamento': lancamento });
  }

  changeModelDataIda($event) {
    this.viagem.dataIda = FormatUtils.datePickerToData(this.viagem.dataIdaPicker);
  }

  changeModelDataVolta($event) {
    this.viagem.dataVolta = FormatUtils.datePickerToData(this.viagem.dataVoltaPicker);
  }

  voltar() {
    this.navCtrl.pop();
  }

  async ionViewDidLoad() {
    let loading = this.loadingCtrl.create({
      content: 'Aguarde, carregando...',
      enableBackdropDismiss: false
    });

    loading.present();

    this.clientes = await this.minhasViagensService.getClientesOffline();
    this.projetos = await this.minhasViagensService.getProjetosOffline();

    // Tenta buscar online os dados
    try {
      this.clientes = await this.minhasViagensService.getClientes();
      this.minhasViagensService.saveClientesOffline(this.clientes);
    } catch (error) {
      // Erro
      this.clientes = await this.minhasViagensService.getClientesOffline();

      this.msg.msgSemConexao();
    }

    try {
      this.projetos = await this.minhasViagensService.getProjetos();
      this.minhasViagensService.saveProjetosOffline(this.projetos);
    } catch (error) {
      // Erro
      this.projetos = await this.minhasViagensService.getProjetosOffline();

      this.msg.msgSemConexao();
    }

    this.isNovo = this.navParams.get('novo');

    if (!this.isNovo) {
      let v: Viagem = null;

      v = this.navParams.get('viagem');

      // Primeiro, verifica se existem os dados offline
      let e = await this.minhasViagensService.buscarOffline(v.id);

      if (!e) {
        v = this.navParams.get('viagem');
      } else {
        v = e.vo;
      }

      // Se não tem, volta a variável
      if (!v || !v.id) {
        v = this.navParams.get('viagem');
      }

      // Busca dados online
      try {
        // Somente busca novos dados, se estiver sincronizado
        if (!e || e.status == 1) {
          v = await this.minhasViagensService.buscar(v.id);
          this.minhasViagensService.saveOffline(v, v.id);
        }
      } catch (error) {
        // Erro
        console.error(error);
        this.msg.msgSemConexao();
      }

      this.tratamentoModel(v);

      this.viagem = v;

      // Formato da data
      this.viagem.dataIdaPicker = FormatUtils.dataToDatePicker(this.viagem.dataIda);
      this.viagem.dataVoltaPicker = FormatUtils.dataToDatePicker(this.viagem.dataVolta);
    } else {
      // Clientes e projetos default, e outros
      this.viagem = {};
      this.viagem.cliente = {};
      this.viagem.cliente.id = -1;

      this.viagem.projeto = {};
      this.viagem.projeto.id = -1;
    }

    loading.dismiss();
  }

  private tratamentoModel(v: Viagem) {
    if (!v.cliente) {
      v.cliente = {};
    }
    if (!v.projeto) {
      v.projeto = {};
    }
  }

  public async tirarFoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let lancamento: Lancamento = {
        recibo: {},
        sequencial: (this.viagem.lancamentos.length + 1).toString(),
        categoria: {},
        tipoPeriodo: 'Data de Referência',
        dataInicial: Utils.getCurrentDate(),
        valorLancamento: 0
      };
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      lancamento.recibo.fotoAnexoRecibo = base64Image;

      this.navegarAvancar(lancamento);

    }, (err) => {
      // Handle error
    });
  }

  public async qrCode() {
    await this.salvar(false);

    //
    this.barcodeScanner.scan({
      formats: 'QR_CODE',

    }).then((barcodeData) => {
      if (!barcodeData.cancelled) {
        let lancamento: Lancamento = {};

        this.minhasViagensService.trataUrlNfe(barcodeData.text, lancamento, this.viagem, true);

        this.navegarAvancar(lancamento);
      }
    }, (err) => console.error(err))
      .catch((err2) => console.error(err2));
  }

}
