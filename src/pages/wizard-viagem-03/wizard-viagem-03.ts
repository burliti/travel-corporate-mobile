import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Utils } from './../../utils/shared';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { MinhasViagensService } from './../../services/minhas-viagens.service';
import { InternetService } from './../../services/internet.service';
import { LancamentoDetalhePage } from './../lancamento-detalhe/lancamento-detalhe';
import { WizardViagem_04Page } from './../wizard-viagem-04/wizard-viagem-04';
import { Lancamento } from './../../models/lancamento.model';
import { Viagem } from './../../models/viagem.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ItemSliding, LoadingController } from 'ionic-angular';
import { MessagesService } from '../../services/messages.service';

@IonicPage()
@Component({
  selector: 'page-wizard-viagem-03',
  templateUrl: 'wizard-viagem-03.html',
})
export class WizardViagem_03Page {

  public isNovo: boolean = true;
  public viagem: Viagem = {
    cliente: {},
    projeto: {},
    fechamento: {},
    lancamentos: []
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public internet: InternetService,
    public minhasViagensService: MinhasViagensService,
    public loadingCtrl: LoadingController,
    public camera: Camera,
    public msg: MessagesService,
    public barcodeScanner: BarcodeScanner) {
  }

  public isPodeEditar() {
    return this.minhasViagensService.isPodeEditarViagem(this.viagem);
  }

  avancar() {
    this.navCtrl.push(WizardViagem_04Page, { 'viagem': this.viagem, 'novo': this.isNovo });
  }

  detalhesLancamento(lancamento: Lancamento, novo: boolean = false) {
    this.navCtrl.push(LancamentoDetalhePage, { 'viagem': this.viagem, 'novo': novo, 'lancamento': lancamento });
  }

  adicionarLancamento(lancamento: Lancamento = null) {
    this.detalhesLancamento(lancamento, true);
  }

  voltar() {
    this.navCtrl.pop();
  }

  async ionViewDidLoad() {
    this.isNovo = this.navParams.get('novo');
    this.viagem = this.navParams.get('viagem');
    let lancamento = this.navParams.get('lancamento');

    if (lancamento) {
      this.adicionarLancamento(lancamento);
    }
  }

  getDataTratada(data) {
    return data ? data : 'não informado';
  }

  async excluirLancamento(item: Lancamento, slidingItem: ItemSliding) {
    slidingItem.close();

    this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja realmente EXCLUIR o lançamento selecionado?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        }, {
          text: 'Confirmar',
          handler: () => {
            this.toastCtrl.create({
              message: 'Excluindo...',
              duration: 1000
            }).present();
            try {
              let index = this.viagem.lancamentos.indexOf(item);
              this.viagem.lancamentos = this.viagem.lancamentos.splice(index, 1);
            } catch (err) {
              this.toastCtrl.create({
                message: JSON.stringify(err),
                duration: 3000
              }).present();
            }

            // this.salvar();
          }
        }
      ]
    }).present();
  }

  async salvar(isAvancar: boolean = false) {
    // Se nao pode editar, apenas avança nas telas...
    if (!this.isPodeEditar()) {
      if (isAvancar) {
        this.avancar();
      }
      return;
    }

    let loading = this.loadingCtrl.create({
      content: 'Aguarde, carregando...',
      enableBackdropDismiss: false
    });

    loading.present();

    // 1 - Tenta salvar online os dados
    if (this.isNovo) {
      try {
        let response = await this.minhasViagensService.inserir(this.viagem);

        if (response.success) {

          this.tratamentoModel(response.vo);

          this.viagem = response.vo;

          this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

          this.isNovo = false;

          if (isAvancar) {
            this.avancar();
          }
        } else {
          this.msg.msgError(response);
        }
      } catch (error) {
        // Erro genérico
        this.msg.msgSemConexao();

        // Salva os dados offline
        this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

        // Avança para a próxima tela
        if (isAvancar) {
          this.avancar();
        }
      }

    } else {
      try {
        let response = await this.minhasViagensService.atualizar(this.viagem, this.viagem.id);

        if (response.success) {
          this.tratamentoModel(response.vo);

          this.viagem = response.vo;

          this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

          if (isAvancar) {
            this.avancar();
          }
        } else {
          this.msg.msgError(response);
        }

      } catch (error) {
        // Erro genérico
        this.msg.msgSemConexao();

        // Salva os dados offline
        this.minhasViagensService.saveOffline(this.viagem, this.viagem.id);

        // Avança para a próxima tela
        if (isAvancar) {
          this.avancar();
        }
      }
    }

    loading.dismiss();
  }

  private tratamentoModel(v: Viagem) {
    if (!v.cliente) {
      v.cliente = {};
    }
    if (!v.projeto) {
      v.projeto = {};
    }
  }

  public async tirarFoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let lancamento: Lancamento = {
        recibo: {},
        sequencial: (this.viagem.lancamentos.length + 1).toString(),
        categoria: {},
        tipoPeriodo: 'Data de Referência',
        dataInicial: Utils.getCurrentDate(),
        valorLancamento: 0
      };
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      lancamento.recibo.fotoAnexoRecibo = base64Image;

      this.adicionarLancamento(lancamento);

    }, (err) => {
      console.log(err);
    });
  }

  public async qrCode() {
    this.barcodeScanner.scan({
      formats: 'QR_CODE',

    }).then((barcodeData) => {
      if (!barcodeData.cancelled) {
        let lancamento: Lancamento = {};

        this.minhasViagensService.trataUrlNfe(barcodeData.text, lancamento, this.viagem, true);

        this.adicionarLancamento(lancamento);
      }
    }, (err) => console.error(err))
      .catch((err2) => console.error(err2));
  }
}
