import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WizardViagem_03Page } from './wizard-viagem-03';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    WizardViagem_03Page,
  ],
  imports: [
    IonicPageModule.forChild(WizardViagem_03Page),
    ComponentsModule
  ]
})
export class WizardViagem_03PageModule { }
