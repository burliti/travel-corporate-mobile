import { AppServicesModule } from './../services/app.services.module';
import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header';

@NgModule({
  declarations: [HeaderComponent,
    HeaderComponent],
  imports: [AppServicesModule],
  exports: [HeaderComponent,
    HeaderComponent]
})
export class ComponentsModule { }
