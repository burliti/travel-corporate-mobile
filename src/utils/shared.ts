
export class Utils {
  static getRandomNumber() {
    return Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
  }

  static getCurrentDate() {
    return Utils.formatDate(new Date());
  }

  static buildErrorMessage(response: any): string {
    //
    let msg = response.message;

    if (response.errors && response.errors.length > 0) {
      msg += '<div class="text-left">';
      msg += '<ul>';

      for (const error of response.errors) {
        msg += '<li>';
        msg += error.message;
        msg += '</li>';
      }
      msg += '</ul>';
      msg += '</div>';
    }

    return msg;
  }

  static parseNumber(value: any) {
    if (!value) {
      return 0.0;
    }

    let v: string = value.toString();

    if (v.indexOf('R$') >= 0) {
      v = v.replace('R$', '');
      v = v.replace('.', '');
      v = v.replace(',', '.');
    }

    let f = parseFloat(v);

    return f;
  }

  static formatDate(data: Date): string {
    let dd = data.getDate().toString();
    let mm = new String(data.getMonth() + 1); //January is 0!
    var yyyy = data.getFullYear();

    if (data.getDate() < 10) {
      dd = '0' + dd
    }

    if (data.getMonth() < 10) {
      mm = '0' + mm
    }

    let retorno = dd + '/' + mm + '/' + yyyy;

    return retorno;
  }

  static hex2a(hexx: string) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
      str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
  }
}
