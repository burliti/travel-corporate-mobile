import { Fornecedor } from './fornecedor.model';
import { Viagem } from './viagem.model';
import { Recibo } from './recibo.model';
import { Funcionario } from './funcionario.model';
import { Categoria } from './categoria.model';

export class Lancamento {
  id?: string;
  sequencial?: string;
  dataHoraLancamento?: string;
  dataInicial?: string;
  dataInicialPicker?: string;
  dataFinal?: string;
  dataFinalPicker?: string;
  observacoes?: string;
  tipoLancamento?: string;
  valorLancamento?: number;
  valorLancamentoString?: string;
  categoria?: Categoria = {};
  funcionario?: Funcionario = {};
  recibo?: Recibo = {};
  viagem?: Viagem = {};
  referenciado?: Lancamento = {};
  fornecedor?: Fornecedor = {};
  tipoPeriodo?: string;
}
