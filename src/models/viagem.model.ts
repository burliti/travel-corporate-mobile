import { Fechamento } from './fechamento.model';
import { Funcionario } from './funcionario.model';
import { Projeto } from './projeto.model';
import { Cliente } from './cliente.model';
import { Lancamento } from './lancamento.model';

export class Viagem {
  id?: string;
  sequencial?: string;
  dataIda?: string;
  dataVolta?: string;

  dataIdaPicker?: string;
  dataVoltaPicker?: string;

  descricao?: string;
  objetivo?: string;
  observacao?: string;
  status?: string;
  statusValue?: string;
  cliente?: Cliente = {};
  projeto?: Projeto = {};
  funcionario?: Funcionario = {};
  lancamentos?: Lancamento[] = [];
  fechamento?: Fechamento = {};
}
