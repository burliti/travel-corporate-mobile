export class Empresa {
  id?: string;
  nome?: string;
  fantasia?: string;
  chaveEmpresa?: string;
}
