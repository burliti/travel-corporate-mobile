import { Page } from 'ionic-angular/navigation/nav-util';

export class Menu {
  constructor(
    public title: string,
    public icon: string,
    public component: Page) {
    this.title = title;
    this.component = component;
  }
}
