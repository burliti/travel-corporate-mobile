import { Acao } from './acao.model';

export class Recurso {
  id?: string;
  nome?: string;
  descricao?: string;
  status?: string;
  icone?: string;
  filhos?: Recurso[];
  acoes?: AcaoRecurso[];
}

export class AcaoRecurso {
  id?: string;
  acao?: Acao = {}
}
