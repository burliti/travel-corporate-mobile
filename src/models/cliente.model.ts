export class Cliente {
  id?: number;
  nome?: string;
  status?: string;
  cidade?:string;
  uf?:string;
  observacoes?:string;
}
