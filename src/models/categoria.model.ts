export class Categoria {
  id?: string;
  interna?: string;
  nome?: string;
  status?: string;
  tipo?: string;
}
