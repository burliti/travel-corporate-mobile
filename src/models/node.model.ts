export class Node {
  id?: string;
  nome?: string;
  url?: string;
  selecionado?: boolean;
  icone?: string;
  filhos?: Node[];
}
