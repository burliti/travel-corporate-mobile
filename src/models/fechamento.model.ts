import { Lancamento } from './lancamento.model';
import { Funcionario } from './funcionario.model';

export class Fechamento {
  id?: string;
  sequencial?: string;

  funcionarioFechamento?: Funcionario;
  funcionarioRecebimento?: Funcionario;
  funcionarioAprovacao?: Funcionario;
  funcionarioRejeicao?: Funcionario;

  dataHoraCadastro?: string;
  dataHoraEnviado?: string;
  dataHoraAprovacao?: string;
  dataHoraRejeicao?: string;

  emailDestino?: string;
  saldoAnterior?: number;
  totalDebitos?: number;
  totalCreditos?: number;
  saldoFinal?: number;
  motivoRejeicao?: string;
  lancamentos?: Lancamento[] = [];
}
