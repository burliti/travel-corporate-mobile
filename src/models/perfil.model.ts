import { Recurso, AcaoRecurso } from './recurso.model';
export class Perfil {
  id?: string;
  nome?: string;
  status?: string;
  recursos?: Recurso[];
  acoes?: AcaoRecurso[];
  nodes?: Node[];
}
