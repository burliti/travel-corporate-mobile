import { Cliente } from './cliente.model';

export class Projeto {
  id?: number;
  nome?: string;
  status?: string;
  valor?: number;
  percentualAlertaGastos?: number;
  percentualLimiteGastos?: number;
  clientes?: Cliente[] = [];
}
