import { Funcionario } from './funcionario.model';
import { Empresa } from './empresa.model';

/**
 * Classe que representa a sessao do usuario no servidor
 */
export class SessaoUsuario {
  chaveSessao?:string;
  empresa?: Empresa;
  dataCriacao?: string;
  ultimaInteracao?: string;
  enderecoIp?: string;
  hostName?: string;
  funcionario?: Funcionario;
}
