import { Funcionario } from './funcionario.model';
import { Fornecedor } from "./fornecedor.model";


export class Recibo {
  id?: string;
  dataInserido?: string;
  dataReferencia?: string;
  dataReferenciaPicker?: string;
  fotoAnexoRecibo?: string;
  numeroDocumento?: string;
  valor?: number;
  valorString?: string;
  urlNfe?: string;
  chaveNfe?: string;
  fornecedor?: Fornecedor = {};
  nomeFornecedor?: string;
  cpfCnpjFornecedor?: string;
  funcionario?: Funcionario = {};
}
