import { Perfil } from './perfil.model';

export class Funcionario {
  id?: string;
  nome?: string;
  administrador?: string;
  resetarSenha?: string;
  status?: string;
  celular?: string;
  email?: string;
  senha?: string;
  confirmacaoSenha?: string;
  perfil?: Perfil;
  foto?: string;
}
