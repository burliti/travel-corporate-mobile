import { PaddingPipe } from './padding.pipe';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [],
  exports: [PaddingPipe],
  declarations: [PaddingPipe],
  providers: [],
})
export class PipeModule { }
