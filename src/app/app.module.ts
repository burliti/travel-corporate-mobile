import { AppServicesModule } from './../services/app.services.module';
import { SobrePageModule } from './../pages/sobre/sobre.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GlobalErrorHandler } from './../services/global-error.handler';
import { Network } from '@ionic-native/network';
import { SolicitacaoPageModule } from './../pages/solicitacao/solicitacao.module';
import { Camera } from '@ionic-native/camera';
import { LOCALE_ID } from '@angular/core';
import { LancamentoDetalhePageModule } from './../pages/lancamento-detalhe/lancamento-detalhe.module';
import { WizardViagem_04PageModule } from './../pages/wizard-viagem-04/wizard-viagem-04.module';
import { WizardViagem_03PageModule } from './../pages/wizard-viagem-03/wizard-viagem-03.module';
import { WizardViagem_02PageModule } from './../pages/wizard-viagem-02/wizard-viagem-02.module';
import { WizardViagem_01PageModule } from './../pages/wizard-viagem-01/wizard-viagem-01.module';
import { MinhasViagensDetalhesPageModule } from './../pages/minhas-viagens-detalhes/minhas-viagens-detalhes.module';
import { LoginPageModule } from './../pages/login/login.module';
import { MinhasViagensPageModule } from './../pages/minhas-viagens/minhas-viagens.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CurrencyUtils } from '../utils/currency-utils';
import { ReciboDetalhePageModule } from '../pages/recibo-detalhe/recibo-detalhe.module';
import { MeusDadosPageModule } from '../pages/meus-dados/meus-dados.module';
import { ListaAvatarPageModule } from '../pages/lista-avatar/lista-avatar.module';
import { AppVersion } from '@ionic-native/app-version';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      mode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out'
    }),
    IonicStorageModule.forRoot({
      name: 'travelcorp',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    AppServicesModule,
    MinhasViagensPageModule,
    MinhasViagensDetalhesPageModule,
    WizardViagem_01PageModule,
    WizardViagem_02PageModule,
    WizardViagem_03PageModule,
    WizardViagem_04PageModule,
    LancamentoDetalhePageModule,
    ReciboDetalhePageModule,
    LoginPageModule,
    SolicitacaoPageModule,
    MeusDadosPageModule,
    ListaAvatarPageModule,
    SobrePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CurrencyUtils,
    Camera,
    BarcodeScanner,
    Network,
    AppVersion,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ]
})
export class AppModule { }
