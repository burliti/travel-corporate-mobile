import { SobrePage } from './../pages/sobre/sobre';
import { MeusDadosPage } from './../pages/meus-dados/meus-dados';
import { HomePage } from './../pages/home/home';
import { Page } from 'ionic-angular/navigation/nav-util';
import { Network } from '@ionic-native/network';
import { LoginService } from './../services/login.service';
import { SessaoUsuario } from './../models/sessao.model';
import { Component, ViewChild } from '@angular/core';
import { Platform, ToastController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from './../pages/login/login';
import { Menu } from '../models/menu.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  public rootPage: any = LoginPage;
  public sessaoUsuario: SessaoUsuario = {};
  public menus: Array<Menu>;
  public currentPage: Page = HomePage;
  @ViewChild(Nav) nav: Nav;
  public foto: string = 'assets/images/sem-foto.jpg';
  public s: Subscription;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loginService: LoginService,
    public toastController: ToastController,
    public network: Network
  ) {
    this.menus = new Array<Menu>();
    this.menus.push(new Menu('Início', 'home', HomePage));
    this.menus.push(new Menu('Meus dados', 'contact', MeusDadosPage));
    this.menus.push(new Menu('Sobre', 'ios-information-circle-outline', SobrePage));

    platform.ready().then(() => {
      statusBar.styleDefault();

      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);

      this.loginService.getUsuarioAutenticado().then((retorno) => {
        this.sessaoUsuario = retorno;

        // Busca a foto
        this.carregarFoto();

        // Inscreve no evento de mudança da foto
        this.loginService.getEventFotoChange().subscribe((novaFoto) => {
          this.foto = novaFoto;
        });

        // Inscreve no evento de logoff
        this.loginService.getEventLogoff().subscribe((mensagem) => {
          this.nav.setRoot(LoginPage);
        });

      }).catch((error) => console.log(error));

      // Monitora a internet do aparelho
      this.inscreverConexaoInternet();
    });

    // Monitora o evento de login
    this.loginService.loginEvent.subscribe((sessao) => {
        this.sessaoUsuario = sessao;
    });
  }

  private carregarFoto() {
    if (this.sessaoUsuario.funcionario && this.sessaoUsuario.funcionario.foto && this.sessaoUsuario.funcionario.foto != '') {
      this.foto = this.sessaoUsuario.funcionario.foto;
    }
    else {
      this.loginService.getFotoUsuario().then((retornoFoto) => {
        if (retornoFoto && retornoFoto != '') {
          this.foto = retornoFoto;
        }
        else {
          this.foto = 'assets/images/sem-foto.jpg';
        }
      });
    }
  }

  tirarFoto() {
    // TODO
  }

  openPage(menu: Menu) {
    if (menu.component != this.currentPage) {
      this.nav.push(menu.component);
    }
  }

  inscreverConexaoInternet() {
    this.s = this.network.onConnect().subscribe(() => {
      this.toastController.create({
        message: 'Conexão reestabelecida!',
        closeButtonText: 'OK',
        showCloseButton: true
      }).present();
    });

    this.network.onDisconnect().subscribe(() => {
      this.toastController.create({
        message: 'Sem conexão à internet!',
        closeButtonText: 'OK',
        showCloseButton: true
      }).present();
    });
  }

  sair() {
    this.loginService.logoff();
  }
}
