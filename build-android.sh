#Gerar o certificado
#keytool -genkey -v -keystore travelcorporate-key.keystore -alias travelcorporate -keyalg RSA -keysize 2048 -validity 10000

# Apagar o apk assinado
rm -Rf TravelCorporate.apk

#Gerar android
ionic cordova build android --device --prod --release --aot

#Assinar o apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore travelcorporate-key.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk travelcorporate

#Compactar
/Users/orlando/Library/Android/sdk/build-tools/27.0.3/zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk TravelCorporate.apk
